package com.apo.enums;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public enum Availability {
    NOT_FILTER("0"),
    FEW_OR_ENOUGH("1"),
    ENOUGH("2");

    private String stringValue;

    Availability(String value) {
        this.stringValue = value;
    }

    public String getStringValue() {
        return stringValue;
    }

    public static Availability fromNumValue(String stringValue) throws IllegalArgumentException{
        for (Availability availability: Availability.values()) {
            if (availability.getStringValue().equalsIgnoreCase(stringValue)) {
                return availability;
            }
        }
        throw new IllegalArgumentException("No constant with value " + stringValue + " found");
    }
}
