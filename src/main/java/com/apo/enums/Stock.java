package com.apo.enums;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public enum Stock {
    NO,
    FEW,
    ENOUGH

}
