package com.apo.enums;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public enum PriceSort {
    NOT_SORT("0"),
    LOW_TO_HIGH("1"),
    HIGH_TO_LOW("2");

    private String stringValue;

    PriceSort(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public static PriceSort fromNumValue(String numValue) throws IllegalArgumentException{
        for (PriceSort priceSort: PriceSort.values()) {
            if (priceSort.getStringValue().equalsIgnoreCase(numValue)) {
                return priceSort;
            }
        }
        throw new IllegalArgumentException("No constant with value " + numValue + " found");
    }


}
