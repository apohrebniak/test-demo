package com.apo.controller;

import com.apo.error.InvalidAvailabilityException;
import com.apo.error.InvalidPriceSortException;
import com.apo.error.NoMpnParamException;
import com.apo.enums.Availability;
import com.apo.model.ProductService;
import com.apo.model.ProductServiceImpl;
import com.apo.model.entity.Deal;
import com.apo.model.entity.ProductAndDeals;
import com.apo.response.ResponseDeal;
import com.apo.enums.PriceSort;
import com.apo.response.Response;
import com.apo.response.Status;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 10/06/2017.
 */
@Path("/restful.api")
public class RestController {

    @Inject
    private ProductService productService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getprice")
    public Response getPrice(
            @QueryParam("mpn") String mpnParam,
            @DefaultValue("0") @QueryParam("availability") String availabilityParam,
            @DefaultValue("0") @QueryParam("pricesort") String priceSortParam) {

        checkMPN(mpnParam);
        Availability availability = parseAvailability(availabilityParam);
        PriceSort priceSort = parsePriceSort(priceSortParam);

        ProductAndDeals productAndDeals = productService.getByMPN(mpnParam);
        productService.filter(productAndDeals, availability);
        productService.sort(productAndDeals, priceSort);

        return buildResponseFromProduct(productAndDeals)
                .setStatus(Status.OK)
                .build();
    }

    private Response.Builder buildResponseFromProduct(ProductAndDeals productAndDeals) {
        Response.Builder builder = new Response.Builder();
        builder.setId(productAndDeals.getId())
                .setMpn(productAndDeals.getMpn());
        for (Deal deal: productAndDeals.getDeals()) {
            builder.addDeal(new ResponseDeal(deal.getId(), deal.getPrice(), deal.getStock()));
        }
        return builder;
    }

    private void checkMPN(String mpn) throws NoMpnParamException {
        if (mpn == null) {
            throw new NoMpnParamException();
        }
    }

    private Availability parseAvailability(String numValue) throws InvalidAvailabilityException{
        Availability availability = null;
        try {
            availability = Availability.fromNumValue(numValue);
        } catch (IllegalArgumentException e) {
            throw new InvalidAvailabilityException();
        }
        return availability;
    }

    private PriceSort parsePriceSort(String numValue) throws InvalidPriceSortException{
        PriceSort priceSort = null;
        try {
            priceSort = PriceSort.fromNumValue(numValue);
        } catch (IllegalArgumentException e) {
            throw new InvalidPriceSortException();
        }
        return priceSort;
    }
}
