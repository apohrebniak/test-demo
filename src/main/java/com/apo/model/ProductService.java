package com.apo.model;

import com.apo.enums.Availability;
import com.apo.enums.PriceSort;
import com.apo.model.entity.ProductAndDeals;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 15/06/2017.
 */
public interface ProductService {
    ProductAndDeals getByMPN(String mpn);
    void filter(ProductAndDeals productAndDeals, Availability availability);
    void sort(ProductAndDeals productAndDeals, PriceSort sort);
    void checkProductAndDeals(ProductAndDeals productAndDeals);
}
