package com.apo.model.redis;

import redis.clients.jedis.Jedis;

import javax.inject.Singleton;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
@Singleton
public class RedisStorage {
    private Jedis jedis;

    public RedisStorage() {
        this.jedis = buildJedis();
    }

    private Jedis buildJedis() {
        return new Jedis(RedisStorageConstants.REDIS_URL, RedisStorageConstants.PORT);
    }

    public Jedis getJedis() {
        return jedis;
    }
}
