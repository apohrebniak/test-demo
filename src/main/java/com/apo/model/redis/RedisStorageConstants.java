package com.apo.model.redis;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public interface RedisStorageConstants {
    String REDIS_URL = "pub-redis-16657.eu-central-1-1.1.ec2.redislabs.com";
    int PORT = 16657;
    String KEY_PREFIX_PRODUCT = "product:";
}
