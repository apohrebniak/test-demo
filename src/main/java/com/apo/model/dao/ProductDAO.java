package com.apo.model.dao;

import com.apo.model.entity.ProductAndDeals;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public interface ProductDAO {
    ProductAndDeals getProductByMPN(String mpn);
}
