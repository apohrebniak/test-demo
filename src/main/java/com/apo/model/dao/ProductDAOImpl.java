package com.apo.model.dao;

import com.apo.model.entity.ProductAndDeals;
import com.apo.model.redis.RedisStorage;
import com.apo.model.redis.RedisStorageConstants;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import java.io.IOException;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public class ProductDAOImpl implements ProductDAO {
    @Inject
    private RedisStorage storage;

    @Override
    public ProductAndDeals getProductByMPN(String mpn) {
        String productStr = getProductString(mpn);
        System.out.println(mpn);
        System.out.println(productStr);
        return parseProductString(productStr);
    }

    private ProductAndDeals parseProductString(String productString) {
        ObjectMapper mapper = new ObjectMapper();
        ProductAndDeals productAndDeals = null;
        try {
            if (productString != null) {
                productAndDeals = mapper.readValue(productString, ProductAndDeals.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productAndDeals;
    }

    private String getProductString(String mpn) {
        return storage.getJedis()
                .get(buildMPNKey(mpn));
    }

    private String buildMPNKey(String mpn) {
        return RedisStorageConstants.KEY_PREFIX_PRODUCT.concat(mpn);
    }
}
