package com.apo.model;

import com.apo.enums.Availability;
import com.apo.enums.PriceSort;
import com.apo.error.MpnNotFoundException;
import com.apo.model.dao.ProductDAO;
import com.apo.model.entity.Deal;
import com.apo.model.entity.ProductAndDeals;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 15/06/2017.
 */
public class ProductServiceImpl implements ProductService{

    private ProductDAO dao;

    @Inject
    public ProductServiceImpl(ProductDAO dao) {
        this.dao = dao;
    }

    @Override
    public ProductAndDeals getByMPN(String mpn) throws MpnNotFoundException {
        ProductAndDeals result = dao.getProductByMPN(mpn);
        checkProductAndDeals(result);
        return result;
    }

    @Override
    public void sort(ProductAndDeals productAndDeals, PriceSort priceSort) {
        Stream<Deal> dealStream = productAndDeals.getDeals().stream();
        List<Deal> sortedAndFilteredDeals = dealStream
                .sorted((d1, d2) -> compareByPriceSort(priceSort, d1, d2))
                .collect(Collectors.toList());
        productAndDeals.setDeals(sortedAndFilteredDeals);
    }

    @Override
    public void filter(ProductAndDeals productAndDeals, Availability availability) {
        Stream<Deal> dealStream = productAndDeals.getDeals().stream();
        List<Deal> sortedAndFilteredDeals = dealStream.filter(deal -> deal.getStock()
                .ordinal() >= availability.ordinal())
                .collect(Collectors.toList());
        productAndDeals.setDeals(sortedAndFilteredDeals);
    }

    private int compareByPriceSort(PriceSort priceSort, Deal d1, Deal d2) {
        switch (priceSort) {
            case HIGH_TO_LOW:
                return -Float.compare(d1.getPrice(), d2.getPrice());
            case LOW_TO_HIGH:
                return Float.compare(d1.getPrice(), d2.getPrice());
        }
        return 0;
    }

    @Override
    public void checkProductAndDeals(ProductAndDeals productAndDeals) throws MpnNotFoundException{
        if (productAndDeals == null) {
            throw new MpnNotFoundException();
        }
    }
}
