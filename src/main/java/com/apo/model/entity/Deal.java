package com.apo.model.entity;

import com.apo.enums.Stock;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public class Deal {
    @JsonProperty("id")
    private int id;
    @JsonProperty("price")
    private float price;
    @JsonProperty("stock")
    private Stock stock;

    public Deal() {
    }

    public Deal(int id, float price, Stock stock) {
        this.id = id;
        this.price = price;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
