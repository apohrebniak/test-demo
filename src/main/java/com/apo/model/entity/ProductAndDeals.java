package com.apo.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public class ProductAndDeals {
    @JsonProperty("mpn")
    private String mpn;
    @JsonProperty("id")
    private int id;
    @JsonProperty("array")
    private List<Deal> deals;

    public ProductAndDeals() {
    }

    public ProductAndDeals(String mpn, int id, List<Deal> deals) {
        this.mpn = mpn;
        this.id = id;
        this.deals = deals;
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Deal> getDeals() {
        return deals;
    }

    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }
}
