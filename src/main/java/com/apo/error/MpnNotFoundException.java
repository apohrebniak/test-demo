package com.apo.error;

import com.apo.response.ErrorResponse;
import com.apo.response.Status;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public class MpnNotFoundException extends WebApplicationException {

    public MpnNotFoundException() {
        super("MPN not found");
    }

    @Override
    public Response getResponse() {
        ErrorResponse response = new ErrorResponse.Builder()
                .setStatus(Status.ERROR)
                .setMsg(getMessage())
                .build();
        return Response.status(Response.Status.NOT_FOUND)
                .entity(response)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
