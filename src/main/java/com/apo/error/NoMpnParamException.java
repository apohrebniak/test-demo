package com.apo.error;

import com.apo.response.ErrorResponse;
import com.apo.response.Status;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public class NoMpnParamException extends WebApplicationException {
    public static final String MSG = "MPN is a mandatory parameter";

    public NoMpnParamException() {
        super("MPN not found");
    }

    @Override
    public Response getResponse() {
        ErrorResponse response = new ErrorResponse.Builder()
                .setStatus(Status.ERROR)
                .setMsg(getMessage())
                .setFullMsg(MSG)
                .build();
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(response)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
