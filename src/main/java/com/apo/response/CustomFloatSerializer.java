package com.apo.response;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 12/06/2017.
 */
public class CustomFloatSerializer extends JsonSerializer<Float>{
    @Override
    public void serialize(Float aFloat, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException, JsonProcessingException {

        if (null == aFloat) {
            //write the word 'null' if there's no value available
            jsonGenerator.writeNull();
        } else {
            NumberFormat format = new DecimalFormat();
            format.setMinimumFractionDigits(2);
            format.setMaximumFractionDigits(2);
            jsonGenerator.writeString(format.format(aFloat));
        }
    }
}
