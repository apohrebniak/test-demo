package com.apo.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 10/06/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    @JsonProperty("mpn")
    private String mpn;
    @JsonProperty("status")
    private Status status;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("array")
    private List<ResponseDeal> responseDeals;

    private Response(Builder builder) {
        this.mpn = builder.getMpn();
        this.status = builder.getStatus();
        this.id = builder.getId();
        this.responseDeals = builder.getResponseDeals();
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ResponseDeal> getResponseDeals() {
        return responseDeals;
    }

    public void setResponseDeals(List<ResponseDeal> responseDeals) {
        this.responseDeals = responseDeals;
    }

    public static class Builder {
        private String mpn;
        private Status status;
        private Integer id;
        private List<ResponseDeal> responseDeals = new ArrayList<>();

        private String getMpn() {
            return mpn;
        }

        public Builder setMpn(String mpn) {
            this.mpn = mpn;
            return this;
        }

        private Status getStatus() {
            return status;
        }

        public Builder setStatus(Status status) {
            this.status = status;
            return this;
        }

        private Integer getId() {
            return id;
        }

        public Builder setId(Integer id) {
            this.id = id;
            return this;
        }

        private List<ResponseDeal> getResponseDeals() {
            return responseDeals;
        }

        public Builder setResponseDeals(List<ResponseDeal> responseDeals) {
            this.responseDeals = responseDeals;
            return this;
        }

        public Builder addDeal(ResponseDeal responseDeal) {
            responseDeals.add(responseDeal);
            return this;
        }

        public Response build() {
            return new Response(this);
        }
    }
}
