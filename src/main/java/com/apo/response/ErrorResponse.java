package com.apo.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 10/06/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    @JsonProperty("msg")
    private String msg;
    @JsonProperty("full_msg")
    private String fullMsg;
    @JsonProperty("status")
    private Status status;

    private ErrorResponse(Builder builder) {
        this.msg = builder.getMsg();
        this.fullMsg = builder.getFullMsg();
        this.status = builder.getStatus();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFullMsg() {
        return fullMsg;
    }

    public void setFullMsg(String fullMsg) {
        this.fullMsg = fullMsg;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public static class Builder {
        private String msg = null;
        private Status status = null;
        private String fullMsg = null;

        public ErrorResponse build() {
            return new ErrorResponse(this);
        }

        public String getMsg() {
            return msg;
        }

        public Builder setMsg(String msg) {
            this.msg = msg;
            return this;
        }

        public Status getStatus() {
            return status;
        }

        public Builder setStatus(Status status) {
            this.status = status;
            return this;
        }

        public String getFullMsg() {
            return fullMsg;
        }

        public Builder setFullMsg(String fullMsg) {
            this.fullMsg = fullMsg;
            return this;
        }
    }
}
