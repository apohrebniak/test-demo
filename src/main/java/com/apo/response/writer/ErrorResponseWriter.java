package com.apo.response.writer;

import com.apo.response.ErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ErrorResponseWriter implements MessageBodyWriter<ErrorResponse> {
    private ObjectMapper mapper = new ObjectMapper();

    public boolean isWriteable(Class<?> aClass, Type type,
                               Annotation[] annotations, MediaType mediaType) {
        return aClass.equals(ErrorResponse.class);
    }

    public long getSize(ErrorResponse errorResponse, Class<?> aClass,
                        Type type, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    public void writeTo(ErrorResponse errorResponse,
                        Class<?> aClass, Type type,
                        Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap,
                        OutputStream outputStream) throws IOException, WebApplicationException {


        try (Writer writer = new PrintWriter(outputStream)){
            writer.write(mapper.writeValueAsString(errorResponse));
        }
    }
}
