package com.apo.response.writer;

import com.apo.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ResponseWriter implements MessageBodyWriter<Response> {
    private ObjectMapper mapper = new ObjectMapper();

    public boolean isWriteable(Class<?> aClass, Type type,
                               Annotation[] annotations, MediaType mediaType) {
        return aClass.equals(Response.class);
    }

    public long getSize(Response errorResponse, Class<?> aClass,
                        Type type, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    public void writeTo(Response errorResponse,
                        Class<?> aClass, Type type,
                        Annotation[] annotations,
                        MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap,
                        OutputStream outputStream) throws IOException, WebApplicationException {


        try (Writer writer = new PrintWriter(outputStream)){
            writer.write(mapper.writeValueAsString(errorResponse));
        }
    }
}
