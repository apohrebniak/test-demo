package com.apo.response;

import com.apo.enums.Stock;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 15/06/2017.
 */
public class CustomStockSerializer extends JsonSerializer<Stock> {

    @Override
    public void serialize(Stock stock, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        if (stock == null) {
            jsonGenerator.writeNull();
        } else {
            jsonGenerator.writeString(String.valueOf(stock.ordinal()));
        }
    }
}
