package com.apo.response;

import com.apo.enums.Stock;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 11/06/2017.
 */
public class ResponseDeal {
    @JsonProperty("id")
    private int id;
    @JsonProperty("price")
    @JsonSerialize(using = CustomFloatSerializer.class)
    private float price;
    @JsonProperty("stock")
    @JsonSerialize(using = CustomStockSerializer.class)
    private Stock stock;

    public ResponseDeal(int id, float price, Stock stock) {
        this.id = id;
        this.price = price;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
}
