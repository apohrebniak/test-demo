package com.apo.response;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 10/06/2017.
 */
public enum Status {
    OK,
    ERROR
}
