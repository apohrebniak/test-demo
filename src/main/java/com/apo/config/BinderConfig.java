package com.apo.config;

import com.apo.model.ProductService;
import com.apo.model.ProductServiceImpl;
import com.apo.model.dao.ProductDAO;
import com.apo.model.dao.ProductDAOImpl;
import com.apo.model.redis.RedisStorage;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 10/06/2017.
 */
public class BinderConfig extends AbstractBinder {
    protected void configure() {
        bind(RedisStorage.class).to(RedisStorage.class).in(Singleton.class);
        bind(ProductDAOImpl.class).to(ProductDAO.class);
        bind(ProductServiceImpl.class).to(ProductService.class);
    }
}
