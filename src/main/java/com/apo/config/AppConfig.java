package com.apo.config;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by Andrii Pohrebniak andrii.pohrebniak@gmail.com on 10/06/2017.
 */
public class AppConfig extends ResourceConfig{
    public AppConfig() {
        super();
        packages("com.apo");
        register(new BinderConfig());
    }
}
